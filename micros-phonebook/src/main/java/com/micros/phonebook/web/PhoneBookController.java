package com.micros.phonebook.web;

import com.micros.phonebook.persistence.Person;
import com.micros.phonebook.service.IPersonService;
import com.micros.phonebook.util.PersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contacts")
public class PhoneBookController {

    @Autowired
    IPersonService personService;

    @GetMapping("/")
    public List<Person> getContacts() {
        return personService.fetchAll();
    }

    @GetMapping("/{personId}")
    public Person getPersonDetails(@PathVariable("personId") Long id) {
        return personService.getPersonDetails(id);
    }

    @PostMapping("/save")
    public String saveContact(@RequestBody PersonDto personDto) {
        personService.savePerson(personDto);
        return "success";
    }
}
