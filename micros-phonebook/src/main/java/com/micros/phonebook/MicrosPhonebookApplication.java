package com.micros.phonebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrosPhonebookApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrosPhonebookApplication.class, args);
	}
}
