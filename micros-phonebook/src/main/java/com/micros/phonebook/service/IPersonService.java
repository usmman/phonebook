package com.micros.phonebook.service;

import com.micros.phonebook.persistence.Person;
import com.micros.phonebook.util.PersonDto;

import java.util.List;

public interface IPersonService {

    void savePerson(PersonDto personDto);
    List<Person> fetchAll();
    Person getPersonDetails(Long id);
}
