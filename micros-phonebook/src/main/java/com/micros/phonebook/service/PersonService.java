package com.micros.phonebook.service;

import com.micros.phonebook.persistence.Person;
import com.micros.phonebook.persistence.PersonRepository;
import com.micros.phonebook.util.PersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService implements IPersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public void savePerson(PersonDto personDto) {
        Person p = new Person();

        if((personDto.getMobile() == null || personDto.getMobile().isEmpty()) &&
                (personDto.getName() == null || personDto.getName().isEmpty()))
            throw new RuntimeException("Name or Mobile needs some value.");

        p.setId(-1);
        p.setName(personDto.getName());
        p.setMobile(personDto.getMobile());
        personRepository.save(p);
    }

    @Override
    public List<Person> fetchAll() {
        return personRepository.findAll();
    }

    @Override
    public Person getPersonDetails(Long id) {
        Person p = personRepository.findOne(id);
        if(p == null)
            throw new RuntimeException("No person found of given identifier");
        return p;
    }
}
