package com.micros.phonebook.persistence;

import javax.persistence.*;

@Entity
@SequenceGenerator(sequenceName="person_id_seq", name = "personSeq")
public class Person {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "personSeq")
    private Long id;
    private String name;
    private String mobile;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
